// ------------------------------------------------------------------------- //
// eslint configuration for this file
//

/* global imports */

/* exported extensionSystemImports */
/* exported registerClass */

// ------------------------------------------------------------------------- //
// enforce strict mode
"use strict";


// ------------------------------------------------------------------------- //
// system libraries imports
const GObject = imports.gi.GObject;


// ------------------------------------------------------------------------- //
const VERSION_LIST = imports.misc.config.PACKAGE_VERSION.split(".");


// ------------------------------------------------------------------------- //
// TODO: change to a function
var registerClass;
{
  if (VERSION_LIST[0] >= 3 && VERSION_LIST[1] > 30) {
    registerClass = GObject.registerClass;
  } else {
    registerClass = (x => x);
  }
}


// ------------------------------------------------------------------------- //
var extensionSystemImports = function extensionSystemImports() {
  // post Gnome Shell 3.30
  if (imports.ui.main.extensionManager !== undefined) {
    return {
      extensionManager: imports.ui.main.extensionManager,
      extensionLookup: (uuid) => {
        return imports.ui.main.extensionManager.lookup(uuid);
      },
      ExtensionState: imports.misc.extensionUtils.ExtensionState
    };
  }
  // gnome 3.28
  else {
    return {
      extensionManager: imports.ui.extensionSystem,
      extensionLookup: (uuid) => {
        if (
          Object.prototype.hasOwnProperty.call(
            imports.misc.extensionUtils.extensions,
            uuid
          ) === true
        ) {
          return imports.misc.extensionUtils.extensions[uuid];
        }
        else {
          return undefined;
        }
      },
      ExtensionState: imports.ui.extensionSystem.ExtensionState
    };
  }
};
