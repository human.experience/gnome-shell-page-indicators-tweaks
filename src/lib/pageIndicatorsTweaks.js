/* ------------------------------------------------------------------------- */
// eslint configuration for this file
//
/* global imports */

/* exported PageIndicatorsTweaks */


/* ------------------------------------------------------------------------- */
// enforce strict mode
"use strict";


/* ------------------------------------------------------------------------- */
// gnome shell imports
const gsExtensionUtils = imports.misc.extensionUtils;


/* ------------------------------------------------------------------------- */
// extension imports
const Extension = gsExtensionUtils.getCurrentExtension();
const Compat = Extension.imports.lib.compat;
const SettingsRegistry = Extension.imports.lib.settingsRegistry;
const Utils = Extension.imports.lib.utils;

const DashToDockIntegration = Extension.imports.lib.dashToDockIntegration;
const PageIndicatorsIntegration = Extension.imports.lib.
  pageIndicatorsIntegration;


/* ------------------------------------------------------------------------- */
var PageIndicatorsTweaks = class PageIndicatorsTweaks {

  /* ....................................................................... */
  constructor() {

    // logger
    this._logger = new Utils.Logger(
      "pageIndicatorsTweaks.js::PageIndicatorsTweaks"
    );

    // signals registry
    this._signalsRegistry = new Utils.SignalsRegistry();

    // injections registry
    this._injectionsRegistry = new Utils.InjectionsRegistry();

    // settings registry
    this._settingsRegistry = new SettingsRegistry.SettingsRegistry();

    // list containing integrations
    this._integrations = [];

  }

  /* ....................................................................... */
  enable() {
    let extensionManager = Compat.extensionSystemImports().extensionManager;

    // initialize registry
    this._settingsRegistry.init();

    let dashToDockIntegration =
      new DashToDockIntegration.DashToDockIntegration();

    // create list of integrations
    this._integrations = [
      // dash-to-dock integration
      dashToDockIntegration,
      // pageindicator integration
      new PageIndicatorsIntegration.PageIndicatorsIntegration(
        this._injectionsRegistry,
        this._settingsRegistry.boundSettings,
        dashToDockIntegration
      )
    ];

    // enable the integration
    for (let integration of this._integrations) {
      integration.enable();
    }

    // somemight get enabled after this extension so run intergations on
    // "extension-state-changed" signal
    this._signalsRegistry.addWithLabel(
      "extensionManager",
      [
        extensionManager,
        "extension-state-changed",
        this._onExtensionStateChanged.bind(this)
      ]
    );

  }

  /* ....................................................................... */
  disable() {

    // disable all the integration
    for (let integration of this._integrations) {
      integration.disable();
    }
    this._integrations = [];

    // disconnect all signals we connected
    this._signalsRegistry.destroy();

    // remove any leftover injections
    this._injectionsRegistry.destroy();

    // destroy settings registry
    this._settingsRegistry.destroy();

  }

  /* ....................................................................... */
  _onExtensionStateChanged(data, extension) {
    let ExtensionState = Compat.extensionSystemImports().ExtensionState;

    // if extension is this extension do not do anything
    if (extension === Extension) {
      return;
    }

    // go over all integrations and enable or disable them if then integration
    // matches the extension changed
    for (let integration of this._integrations) {
      if (integration.matches(extension.uuid)) {
        if(extension.state === ExtensionState.ENABLED){
          integration.enable();
        }
        else if (extension.state === ExtensionState.DISABLED) {
          integration.disable();
        }
      }
    }
  }

};
