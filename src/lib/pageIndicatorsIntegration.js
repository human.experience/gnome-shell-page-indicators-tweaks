/* ------------------------------------------------------------------------- */
// eslint configuration for this file
//

/* global imports */

/* exported PageIndicatorsIntegration */


/* ------------------------------------------------------------------------- */
// enforce strict mode
"use strict";


/* ------------------------------------------------------------------------- */
// library imports
const Clutter = imports.gi.Clutter;


/* ------------------------------------------------------------------------- */
// gnome shell imports
const gsExtensionUtils = imports.misc.extensionUtils;
const gsAppDisplay = imports.ui.appDisplay;
const gsMain = imports.ui.main;
const gsPageIndicator = imports.ui.pageIndicators;


/* ------------------------------------------------------------------------- */
// extension imports
const Extension = gsExtensionUtils.getCurrentExtension();
const BaseIntegration = Extension.imports.lib.baseIntegration;
const Utils = Extension.imports.lib.utils;


/* ------------------------------------------------------------------------- */
// globals
const POSITION = {
  RIGHT: 0,
  LEFT: 1,
  VERTICALLY_OPPOSITE_OF_DASH_TO_DOCK: 2
};


/* ------------------------------------------------------------------------- */
var PageIndicatorsIntegration = class PageIndicatorsIntegration
  extends BaseIntegration.BaseIntegration {

  /* ....................................................................... */
  constructor(injectionsRegistry, boundSettings, dashToDockIntegration) {

    // call parent constructor
    super();

    // logger
    this._logger = new Utils.Logger(
      "pageIndicatorsIntegration.js::PageIndicatorsIntegration"
    );

    this._enabled = false;

    // bound settings
    this._boundSettings = boundSettings;

    // injections registry
    this._injectionsRegistry = injectionsRegistry;

    // dash to dock integration
    this._dashToDockIntegration = dashToDockIntegration;

    // signals registry
    this._signalsRegistry = new Utils.SignalsRegistry();

    // injection registry name for this class
    this._injectionRegistryName = "pageIndicatorsIntegration";

  }

  /* ....................................................................... */
  matches() {
    return true;
  }

  /* ....................................................................... */
  enable() {
    if (this._enabled === false) {
      this._injectionsRegistry.addWithLabel(
        this._injectionRegistryName,
        [
          gsPageIndicator.AnimatedPageIndicators.prototype,
          "animateIndicators",
          this._animateIndicatorsInjection(
            gsPageIndicator.AnimatedPageIndicators.prototype.animateIndicators
          )
        ]
      );
      this._enabled = true;
    }
  }

  /* ....................................................................... */
  disable() {
    // remove injections
    this._injectionsRegistry.removeWithLabel(this._injectionRegistryName);
    // reset any existing positio
    gsMain.overview.viewSelector.appDisplay._views[gsAppDisplay.Views.ALL]
      .view._pageIndicators.x_align = Clutter.ActorAlign.END;
    this._enabled = false;
  }


  /* ....................................................................... */
  _animateIndicatorsInjection(originalAnimateIndicators) {
    let settings = this._boundSettings;
    let dashToDockIntegration = this._dashToDockIntegration;

    return function animateIndicators(animationDirection) {
      const defaultPosition = Clutter.ActorAlign.END;
      let position;

      switch (settings._position) {
      case POSITION.RIGHT:
        position = Clutter.ActorAlign.END;
        break;
      case POSITION.LEFT:
        position = Clutter.ActorAlign.START;
        break;
      case POSITION.VERTICALLY_OPPOSITE_OF_DASH_TO_DOCK:
        if (dashToDockIntegration.boundSettings === null) {
          position = defaultPosition;
        }
        else {
          switch (dashToDockIntegration.boundSettings._dockPosition){
          case dashToDockIntegration.DOCK_POSITION.RIGHT:
            position = Clutter.ActorAlign.START;
            break;
          case dashToDockIntegration.DOCK_POSITION.LEFT:
            position = Clutter.ActorAlign.END;
            break;
          default:
            // if neither left or right then do default
            position = defaultPosition;
            break;
          }
        }
      }

      // assign position
      this.x_align = position;

      return originalAnimateIndicators.apply(
        this,
        [animationDirection]
      );
    };

  }

};
