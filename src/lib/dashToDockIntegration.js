/* ------------------------------------------------------------------------- */
// eslint configuration for this file
//

/* global imports */

/* exported DashToDockIntegration */
/* exported DOCK_POSITION */


/* ------------------------------------------------------------------------- */
// enforce strict mode
"use strict";


/* ------------------------------------------------------------------------- */
// gnome shell imports
const gsExtensionUtils = imports.misc.extensionUtils;


/* ------------------------------------------------------------------------- */
// extension imports
const Extension = gsExtensionUtils.getCurrentExtension();
const BaseIntegration = Extension.imports.lib.baseIntegration;
const Compat = Extension.imports.lib.compat;
const DashToDockSettingsRegistry = Extension.imports.lib
  .dashToDockSettingsRegistry;
const Utils = Extension.imports.lib.utils;


/* ------------------------------------------------------------------------- */
// globals
const dashToDockExtensionUuid = "dash-to-dock@micxgx.gmail.com";
// dash-to-dock dock-position enum


/* ------------------------------------------------------------------------- */
var DashToDockIntegration = class DashToDockIntegration
  extends BaseIntegration.BaseIntegration {

  get DOCK_POSITION() {
    return {
      TOP: 0,
      RIGHT: 1,
      BOTTOM: 2,
      LEFT: 3
    };
  }

  /* ....................................................................... */
  constructor() {

    // call parent constructor
    super();

    // logger
    this._logger = new Utils.Logger(
      "dashToDockIntegration.js::DashToDockIntegration"
    );

    // reference to dash to dock extension
    this._dashToDockExtension = null;

    // dash-to-dick settings registry
    this._dashToDockSettingsRegistry = null;

    // injection registry name for this class
    this._injectionRegistryName = "dashToDockIntegration";

  }

  /* ....................................................................... */
  matches(uuid) {
    return uuid === dashToDockExtensionUuid;
  }

  /* ....................................................................... */
  enable() {
    let dashToDock;

    let extensionLookup = Compat.extensionSystemImports().extensionLookup;

    // check if we already have an extension reference
    if (this._dashToDockExtension === null) {
      // get the extension
      dashToDock = extensionLookup(dashToDockExtensionUuid);

      if (dashToDock === undefined) {
        dashToDock = null;
      }
      else {
        // store the extension
        this._dashToDockExtension = dashToDock;

      }
    }
    else {
      dashToDock = this._dashToDockExtension;
    }

    if (this._dashToDockSettingsRegistry === null) {
      if (dashToDock !== null) {
        // signals registry
        this._dashToDockSettingsRegistry =
          new DashToDockSettingsRegistry.DashToDockSettingsRegistry(
            dashToDock
          );
        // initialize the reg
        this._dashToDockSettingsRegistry.init();

      }
      else {
        this._dashToDockSettingsRegistry = null;
      }
    }

  }

  /* ....................................................................... */
  get boundSettings() {
    if (this._dashToDockSettingsRegistry !== null) {
      return this._dashToDockSettingsRegistry._boundSettings;
    }
    else {
      return null;
    }
  }

  /* ....................................................................... */
  disable() {
    if (this._dashToDockExtension !== null) {
      // remove signals
      if (this._dashToDockSettingsRegistry !== null) {
        this._dashToDockSettingsRegistry.destroy();
        this._dashToDockSettingsRegistry = null;
      }
      // remove injections
      this._dashToDockExtension = null;
    }
  }


};
