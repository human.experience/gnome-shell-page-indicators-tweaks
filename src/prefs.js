/* ------------------------------------------------------------------------- */
// eslint configuration for this file
//
/* global imports */

/* exported buildPrefsWidget */
/* exported init */


/* ------------------------------------------------------------------------- */
// enforce strict mode
"use strict";


/* ------------------------------------------------------------------------- */
// language libraries
//const Lang = imports.lang;


/* ------------------------------------------------------------------------- */
// system libraries imports
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;


/* ------------------------------------------------------------------------- */
// gnome shell imports
const gsExtensionUtils = imports.misc.extensionUtils;


/* ------------------------------------------------------------------------- */
// gnome shell imports
const Extension = gsExtensionUtils.getCurrentExtension();


/* ------------------------------------------------------------------------- */
// extension imports
const Convenience = Extension.imports.lib.convenience;
const Utils = Extension.imports.lib.utils;

/* ------------------------------------------------------------------------- */
function init() {
}


/* ------------------------------------------------------------------------- */
function buildPrefsWidget() {

  let preferencesContainer;

  // create preferences container
  preferencesContainer = new PreferencesContainer();

  // show preferences container
  preferencesContainer.showAll();

  // return preferences top level widget to be embedded into preferences window
  return preferencesContainer.getTopLevelWidget();
}


/* ------------------------------------------------------------------------- */
class PreferencesContainer {

  /* ....................................................................... */
  constructor() {

    let settingsSchemaId;
    let preferencesGladeFilePath;

    // initialize preferences logger
    this._logger = new Utils.Logger("prefs.js:PreferencesContainer");

    // get extension setting schema id
    settingsSchemaId = Extension.metadata["settings-schema"];

    // get settings object
    this._settings = Convenience.getSettings(settingsSchemaId);

    // compose preferences.glade path
    preferencesGladeFilePath = GLib.build_filenamev([
      Extension.dir.get_path(),
      "ui",
      "preferences.glade",
    ]);

    // create builder from preferences glade file
    this._builder = Gtk.Builder.new_from_file(preferencesGladeFilePath);

    // get top level widget
    this._topLevelWidget = this._builder.get_object("preferences_viewport");

    // bind settings
    this._bindSettings();

  }

  /* ....................................................................... */
  showAll() {

    // set extension_version element to metadata version
    this._builder.get_object("extension_version").label =
      Extension.metadata["version"].toString();

    // show top level widget and it's children except those that have
    // show_all set to false
    this._topLevelWidget.show_all();
  }

  /* ....................................................................... */
  getTopLevelWidget() {
    // return top level widget
    return this._topLevelWidget;
  }

  /* ....................................................................... */
  _bindSettings() {
    this._bindGeneralTabSettings();
  }

  /* ....................................................................... */
  _bindGeneralTabSettings() {

    // bind enable settings to comboboxtex element via entry buffer
    this._bindSettingsToComboBoxTextChangeViaEntryBuffer(
      [
        [
          "position",
          "position_comboboxtext",
          "position_comboboxtext_entrybuffer",
        ]
      ]
    );

    // bind reset settings buttons
    this._bindSettingsResetsToToolButton(
      [
        ["position", "position_reset_button"]
      ]
    );

  }

  /* ....................................................................... */
  _bindSettingsToElementProperty(
    settingsToElements,
    propertyName,
    bindFlags=Gio.SettingsBindFlags.DEFAULT) {

    // go over each setting id and element Id set and bind the two
    for (let [settingId, elementId] of settingsToElements) {
      this._settings.bind(
        settingId,
        this._builder.get_object(elementId),
        propertyName,
        bindFlags
      );
    }
  }

  /* ....................................................................... */
  _bindSettingsToComboBoxTextChangeViaEntryBuffer(settingsToElements) {

    // go over each setting id and element Id set and bind the two
    for (let [settingId, elementId, entryBufferElementId]
      of settingsToElements) {

      // create binding between setting and entry buffer component
      this._settings.bind(
        settingId,
        this._builder.get_object(entryBufferElementId),
        "text",
        Gio.SettingsBindFlags.DEFAULT
      );

      // bind EntryBuffer of updating text property to update ComboBoxText
      this._builder.get_object(entryBufferElementId).connect(
        "notify::text",
        this._entryBufferToComboBoxTextHandlerFactory(elementId, settingId)
      );

      // connect element to settings
      this._builder.get_object(elementId).connect(
        "changed",
        (widget) => {
          this._settings.set_enum(
            settingId,
            widget.get_active()
          );
        }
      );

      // set ui element to existing setting
      this._builder.get_object(elementId).set_active(
        this._settings.get_enum(settingId)
      );

    }
  }

  /* ....................................................................... */
  _entryBufferToComboBoxTextHandlerFactory(comboBoxTextId, settingId) {

    let eventHandlerFunc;

    // create  handler function to handle EntryBuffer element updates
    eventHandlerFunc = function _comboBoxTextToEntryBufferHandler()
    {
      this._builder.get_object(comboBoxTextId).set_active(
        this._settings.get_enum(settingId)
      );

    };

    // return "this" bound event handler
    return eventHandlerFunc.bind(this);

  }


  /* ....................................................................... */
  _bindSettingsResetsToToolButton(settingsToElements) {

    // go over each element id and setting id set and bind the tw
    for (let [settingId, elementId] of settingsToElements) {
      this._builder.get_object(elementId).connect(
        "clicked",
        () => {
          this._settings.reset(settingId);
        }
      );
    }
  }

}
