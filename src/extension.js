/* ------------------------------------------------------------------------- */
// eslint configuration for this file
//
/* global imports */
/* global window */

/* exported init */
/* exported disable */
/* exported enable */


/* ------------------------------------------------------------------------- */
// enforce strict mode
"use strict";


/* ------------------------------------------------------------------------- */
// enable global used for debugging
window.pageIndicatorsTweaks = {
  debug: false,
};


/* ------------------------------------------------------------------------- */
// gnome shell imports
const ExtensionUtils = imports.misc.extensionUtils;


/* ------------------------------------------------------------------------- */
// extension imports
const Extension = ExtensionUtils.getCurrentExtension();
const PageIndicatorsTweaks = Extension.imports.lib.pageIndicatorsTweaks;


/* ------------------------------------------------------------------------- */
/* globals */
let pageIndicatorsTweaksInstance;


/* ------------------------------------------------------------------------- */
function init() {
  pageIndicatorsTweaksInstance = null;
}


/* ------------------------------------------------------------------------- */
function enable() {
  pageIndicatorsTweaksInstance =
    new PageIndicatorsTweaks.PageIndicatorsTweaks();
  pageIndicatorsTweaksInstance.enable();
}


/* ------------------------------------------------------------------------- */
function disable() {
  pageIndicatorsTweaksInstance.disable();
  pageIndicatorsTweaksInstance = null;
}
