# Page Indicators Tweaks

Tweak position of Application view page indicators

![screenshot](https://codeberg.org/human.experience/gnome-shell-page-indicators-tweaks/raw/branch/master/media/view1.png)

## Features

* Move Application View page indicators (dots) to either left of right
* Dash-To-Dock integration, to move page indicator vertically opposite of the dock

## Requirements

Gnome Shell

* 3.34
* 3.36

## Installation

Gnome Extensions Site: [https://extensions.gnome.org/extension/NNNN/page-indicators-tweaks/](https://extensions.gnome.org/extension/2802/page-indicators-tweaks/)

## Usage

Click application icon on your dock to show windows of only that type

## Debugging and Development

If you encounter a problem you can enable the debug logs with:
```
dbus-send \
  --session \
  --type=method_call \
  --dest=org.gnome.Shell \
  /org/gnome/Shell \
  org.gnome.Shell.Eval string:"
  window.pageIndicatorsTweaks.debug = true;
  " \
;
```

Then tail the logs using:
```
journalctl \
  /usr/bin/gnome-shell \
  --follow \
  --output=cat \
| grep "\[page-indicators-tweaks\]" \
;
```

### Development tool-chain

Most development tasks can be performed use included Makefile.

#### System prerequisites

Development can be done using nested Xorg Xephyr session + gnome-shell or
directly forked off gnome-shell in Wayland.

Xephyr is available in most distributions, please use your package manager
to install it.

Additionally NodeJS >= 11.x are used during development. Make sure it is
installed as well using your package manager and available in the PATH.

#### Makefile

A handy Makefile bootstrap capable of handling most development, once above
system prerequisites are installed. It's strongly suggested you use the
Makefile as it sets up a sandboxed Gnome Shell Extension development
environment.

Set up development tooling using:
```
make develop
```

Now you can make the installed tooling available in your PATH by sourcing
the generated `activate.sh` This will also prepend your terminals BASH
prompt with project name for easier recognition.


Activate the development environment:
```
source activate.sh
```

Note: to restore your environment run:
```
page_indicators_tweaks_deactivate
```

To test extension in X use:
```
make x11
```

To test extension in Wayland use:
```
make wayland
```

To build the extension use:
```
make build
```

To build a distribution (zip archive) use:
```
make dist
```

#### Linting

Any code submission will need to be linted against standards in the repository.
Project ESLint specifications are located in `.eslintrc.json`.

To lint code use:
```
make lint
```

## References

* [https://gjs-docs.gnome.org/](https://gjs-docs.gnome.org/)
* [https://wiki.gnome.org/Projects/GnomeShell/Development](https://wiki.gnome.org/Projects/GnomeShell/Development)
* [https://developer.gnome.org](https://developer.gnome.org)
* [https://github.com/zhanghai/gnome-shell-extension-es6-class-codemod](https://github.com/zhanghai/gnome-shell-extension-es6-class-codemod)

